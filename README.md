# TRIAGE Stack

▶&nbsp; __Version:__ 0.0.1  
▶&nbsp; __Repo:__ <https://gitlab.com/op8d/triage_stack/>  

__[TRIAGE Stack](https://gitlab.com/op8d/triage_stack/)__ is a cross-platform
front end architecture which splits an application into six parts:
1. __Text:__ &nbsp; <small>
   A simple plain-text output mode, and a [Text User Interface](
   https://en.wikipedia.org/wiki/Text-based_user_interface) mode</small>
2. __Rust:__ &nbsp; <small>
   A platform-agnostic `App` inside a platform-specific wrapper</small>
3. __I/O:__ &nbsp; <small>
   Deals with patchy internet, and environments with no file system</small>
4. __Audio:__ &nbsp; <small>
   Glitch-free responsive sound, with smart caching and synthesis</small>
5. __Graphics:__ &nbsp; <small>
   Balances CPU and GPU load, using modern graphics card features</small>
6. __ES.Next:__ &nbsp; <small>
   Works hand-in-hand with browser frameworks like Vue and React</small>


## __Crates__

---

### TRIAGE Core

▶&nbsp; __Version:__ 0.0.1  
▶&nbsp; __Repo:__ <https://gitlab.com/op8d/triage/>  
▶&nbsp; __Crate:__ _@TODO just named `triage`_

The __TRIAGE Core__ crate provides everything you need to develop a simple
‘one-shot’ command line app, which:  
<small>`Takes command line arguments` &nbsp; <big>`➔`</big>  &nbsp; `Processes them` &nbsp; <big>`➔`</big>  &nbsp; `Outputs a plain-text result` &nbsp; <big>`➔`</big>  &nbsp; `Exits`</small>

TRIAGE Core is lightweight and completely self contained — it will never depend
on other crates.  
That means:
- __Reduced compile times:__ &nbsp; <small>
  Less work for &nbsp;`cargo build`</small>
- __Unit tests run faster:__ &nbsp; <small>
  Less work for &nbsp;`cargo test`</small>
- __Better [code completion](
  https://en.wikipedia.org/wiki/Intelligent_code_completion) performance:__ &nbsp; <small>
  Tools like [IntelliSense](https://code.visualstudio.com/docs/editor/intellisense)
  run faster</small>
- __Fewer bugs:__ &nbsp; <small>
  Your code is easier to reason about</small>
- __Less maintenance:__ &nbsp; <small>
  Your app is more likely to compile in 10 years’ time</small>

Apps developed using TRIAGE Core have three main advantages over ‘vanilla’ apps
built from scratch:

#### __1. Structured:__

TRIAGE Core is designed to help you hit the ground running. It provides a
standard framework, familiar to developers who have worked on other TRIAGE apps.
- __`App`:__ &nbsp; <small>
  Contains your app’s platform-agnostic business logic</small>
- __`AppCli`:__ &nbsp; <small>
  Wraps `App` for simple ‘one shot’ (non-interactive) command line usage</small>
- __`Fail`:__ &nbsp; <small>
  The standard way of handling errors in TRIAGE apps</small>
- __`Help`:__ &nbsp; <small>
  Builds your app’s `--help` pages</small>
- __`Settings`:__ &nbsp; <small>
  Parses and stores the user’s command line arguments</small>
- __`Widget`:__ &nbsp; <small>
  Encapsulates your app’s functionality into manageable chunks</small>
- __`Xray`:__ &nbsp; <small>
  Handy tools for debugging and optimising performance</small>

#### __2. Portable:__
Your app will perform identically on Linux, MacOS and Windows.  
_@TODO Android? iOS? Browser? Lambda?_

#### __3. Progressive:__
A simple command line app built with TRIAGE Core can become the foundation for a
much more complex app.  
For example, you could use:
- __[TRIAGE Tui](#triage-tui)__ &nbsp; <small>to add an interactive
  [Text User Interface](https://en.wikipedia.org/wiki/Text-based_user_interface)
  mode</small>
- __[TRIAGE Gui](#triage-gui)__ &nbsp; <small>to add a 3D
  [Graphical User Interface](https://en.wikipedia.org/wiki/Graphical_user_interface)
  mode</small>
- __[TRIAGE Fs](#triage-fs)__ &nbsp; <small>to add a ‘Save as PNG’ Widget</small>
- __[TRIAGE Web](#triage-web)__ &nbsp; <small>to make your app run in web browsers</small>


---

### TRIAGE Fs

▶&nbsp; __Version:__ 0.0.1  
▶&nbsp; __Repo:__ <https://gitlab.com/op8d/triage_fs/>  
▶&nbsp; __Crate:__ _@TODO named `triage_fs`_

_@TODO describe_


---

### TRIAGE Gui

▶&nbsp; __Version:__ 0.0.1  
▶&nbsp; __Repo:__ <https://gitlab.com/op8d/triage_gui/>  
▶&nbsp; __Crate:__ _@TODO named `triage_gui`_

_@TODO describe_


---

### TRIAGE Tui

▶&nbsp; __Version:__ 0.0.1  
▶&nbsp; __Repo:__ <https://gitlab.com/op8d/triage_tui/>  
▶&nbsp; __Crate:__ _@TODO named `triage_tui`_

_@TODO describe_


---

### TRIAGE Web

▶&nbsp; __Version:__ 0.0.1  
▶&nbsp; __Repo:__ <https://gitlab.com/op8d/triage_web/>  
▶&nbsp; __Crate:__ _@TODO named `triage_web`_

_@TODO describe_
